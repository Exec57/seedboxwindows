﻿using Seedbox.Services;
using Seedbox.Windows;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Seedbox
{
    public partial class App : Application
    {
        public static HttpService HttpService { get; private set; }
        public static SettingsService SettingsService { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            SettingsService = new SettingsService();
            SettingsService.Read();

            HttpService = new HttpService();

            base.OnStartup(e);
        }
    }
}
