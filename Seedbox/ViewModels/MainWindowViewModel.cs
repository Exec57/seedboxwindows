﻿using Seedbox.Helpers;
using Seedbox.Services;
using Seedbox.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Seedbox.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public static MainWindowViewModel instance { get; private set; }

        private readonly Window window;
        private readonly HttpService http;
        private readonly int maximizedCaption = 7;

        private object currentView;
        public object CurrentView { get => currentView; set => OnPropertyChanged(ref currentView, value); }

        private string title;
        public string Title { get => title; set => OnPropertyChanged(ref title, value); }

        public int TitleHeight { get; private set; } = 29;
        public bool IsWindowMax { get => window.WindowState == WindowState.Maximized; }
        public int CaptionHeight { get => IsWindowMax ? TitleHeight + maximizedCaption : TitleHeight; }
        public int ResizeBorderThickness { get => IsWindowMax ? 0 : 10; }
        public Thickness GridMargin { get => IsWindowMax ? new Thickness(maximizedCaption, maximizedCaption, maximizedCaption, 0) : new Thickness(ResizeBorderThickness); }

        public ICommand ExitCommand { get; private set; }
        public ICommand MinimizeCommand { get; private set; }
        public ICommand MaximizeCommand { get; private set; }

        public MainWindowViewModel(Window window)
        {
            instance = this;

            http = App.HttpService;

            this.window = window;

            window.MaxHeight = SystemParameters.WorkArea.Height + 7;

            window.StateChanged += (s, e) =>
            {
                OnPropertyChanged(nameof(IsWindowMax));
                OnPropertyChanged(nameof(CaptionHeight));
                OnPropertyChanged(nameof(ResizeBorderThickness));
                OnPropertyChanged(nameof(GridMargin));
            };

            var settings = App.SettingsService;
            window.SizeChanged += (s, e) =>
            {
                if (window.WindowState == WindowState.Normal)
                {
                    settings.WindowWidth = window.Width;
                    settings.WindowHeight = window.Height;
                }
               
                settings.WindowMaximized = window.WindowState == WindowState.Maximized;
                settings.Write();
            };

            window.PreviewMouseLeftButtonDown += Window_PreviewMouseButtonDown;
            window.PreviewMouseRightButtonDown += Window_PreviewMouseButtonDown;

            window.Width = settings.WindowWidth;
            window.Height = settings.WindowHeight;
            window.WindowState = settings.WindowMaximized ? WindowState.Maximized : WindowState.Normal;

            UpdateTitle(null);

            CurrentView = new LoginViewModel();
            
            ExitCommand = new RelayCommand(window.Close);
            MinimizeCommand = new RelayCommand(() => window.WindowState = WindowState.Minimized);
            MaximizeCommand = new RelayCommand(() => window.WindowState ^= WindowState.Maximized);
        }

        private void Window_PreviewMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (CurrentView is TorrentsViewModel view)
            {
                var hit = VisualTreeHelper.HitTest(window, e.GetPosition(window));
                var hitType = hit.VisualHit.GetType();

                var textblock = hit.VisualHit as TextBlock;

                if (hitType != typeof(StackPanel) && (textblock == null || textblock.Name != "IconButton"))
                    view.SelectedTorrent = null;
            }
        }

        public void UpdateTitle(string auth)
        {
            Title = "Seedbox";

            if (string.IsNullOrWhiteSpace(auth))
                return;

            var decode = Utility.Base64Decode(auth);
            var tab = decode.Split(':');

            Title += " - " + tab[0];
        }
    }
}
