﻿using Seedbox.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seedbox.Models
{
    public class Torrent : BaseViewModel
    {
        public string Hash { get; set; }

        private string name;
        public string Name { get => name; set => OnPropertyChanged(ref name, value); }

        private string size;
        public string Size { get => size; set => OnPropertyChanged(ref size, value); }

        private int percent;
        public int Percent { get => percent; set => OnPropertyChanged(ref percent, value); }

        private string download;
        public string Download { get => download; set => OnPropertyChanged(ref download, value); }

        private string send;
        public string Send { get => send; set => OnPropertyChanged(ref send, value); }

        private double ratio;
        public double Ratio { get => ratio; set => OnPropertyChanged(ref ratio, value); }

        private string reception;
        public string Reception { get => reception; set => OnPropertyChanged(ref reception, value); }

        private string timeLeft;
        public string TimeLeft { get => timeLeft; set => OnPropertyChanged(ref timeLeft, value); }
    }
}
