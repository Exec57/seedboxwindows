﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Seedbox.Windows
{
    public partial class ConfirmWindow : Window
    {
        static bool choice;

        public ConfirmWindow()
        {
            InitializeComponent();

            KeyDown += ConfirmWindow_KeyDown;
        }

        private void ConfirmWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
            else if (e.Key == Key.Enter)
            {
                choice = true;
                Close();
            }
        }

        public static bool Show(string message)
        {
            choice = false;

            var window = new ConfirmWindow();
            window.MessageTextBlock.Text = message;
            window.ShowDialog();

            return choice;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var content = button.Content as string;

            choice = content == "Oui" ? true : false;
            Close();
        }
    }
}
