﻿using Seedbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Seedbox.Helpers
{
    public static class Utility
    {
        public static bool InDesignMode()
        {
            return !(Application.Current is App);
        }

        public static void RaiseCanExecuteChanged()
        {
            if (Application.Current.Dispatcher.Thread == Thread.CurrentThread)
                CommandManager.InvalidateRequerySuggested();
            else
                Application.Current.Dispatcher.Invoke(CommandManager.InvalidateRequerySuggested);
        }

        public static async Task RaiseCanExecuteChangedAsync()
        {
            if (Application.Current.Dispatcher.Thread == Thread.CurrentThread)
                CommandManager.InvalidateRequerySuggested();
            else
                await Application.Current.Dispatcher.InvokeAsync(CommandManager.InvalidateRequerySuggested);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
