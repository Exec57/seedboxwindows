﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seedbox.Services
{
    public class SettingsService
    {
        private readonly string file = AppDomain.CurrentDomain.BaseDirectory + "/Settings.ini";

        public string ApiUrl = "https://seedbox.exec57.fr/api";
        public string Authorization;
        public double WindowWidth = 1200;
        public double WindowHeight = 700;
        public bool WindowMaximized;

        public void Read()
        {
            try
            {
                string text = File.ReadAllText(file);
                var lines = text.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    var args = line.Split(new[] { '=' }, 2);
                    switch(args[0])
                    {
                        case nameof(ApiUrl):
                            ApiUrl = args[1];
                            break;
                        case nameof(Authorization):
                            Authorization = args[1];
                            break;
                        case nameof(WindowWidth):
                            WindowWidth = double.Parse(args[1]);
                            break;
                        case nameof(WindowHeight):
                            WindowHeight = double.Parse(args[1]);
                            break;
                        case nameof(WindowMaximized):
                            WindowMaximized = bool.Parse(args[1]);
                            break;
                    }
                }
            }
            catch { }
        }

        public void Write()
        {
            try
            {
                using (var sw = new StreamWriter(file))
                {
                    sw.WriteLine($"{nameof(ApiUrl)}={ApiUrl}");
                    sw.WriteLine($"{nameof(Authorization)}={Authorization}");
                    sw.WriteLine($"{nameof(WindowWidth)}={WindowWidth}");
                    sw.WriteLine($"{nameof(WindowHeight)}={WindowHeight}");
                    sw.WriteLine($"{nameof(WindowMaximized)}={WindowMaximized}");
                }
            }
            catch { }
        }
    }
}
