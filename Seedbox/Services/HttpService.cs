﻿using Newtonsoft.Json;
using Seedbox.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seedbox.Services
{
    public class HttpService
    {
        private const string authorizationHeader = "Authorization";

        HttpClient client;

        SettingsService settings;

        public HttpService()
        {
            settings = App.SettingsService;

            client = new HttpClient();
        }

        public async Task<bool> LoginAsync(string auth)
        {
            client.DefaultRequestHeaders.Add(authorizationHeader, $"Basic {auth}");
            var response = await client.PostAsync($"{settings.ApiUrl}/auth/login", null);

            if (!response.IsSuccessStatusCode)
                client.DefaultRequestHeaders.Remove(authorizationHeader);

            return response.IsSuccessStatusCode;
        }

        public void Disconnect()
        {
            client.DefaultRequestHeaders.Remove(authorizationHeader);
        }

        public async Task<List<Torrent>> GetTorrentsAsync()
        {
            var response = await client.GetAsync($"{settings.ApiUrl}/torrents");

            var json = await response.Content.ReadAsStringAsync();
            var torrents = JsonConvert.DeserializeObject<List<Torrent>>(json);

            return torrents;
        }

        public async Task<Torrent> AddFileTorrentAsync(string path)
        {
            var bytes = File.ReadAllBytes(path);

            var content = new MultipartFormDataContent();
            var fileName = Path.GetFileName(path);
            content.Add(new StreamContent(new MemoryStream(bytes)), "torrent", fileName);

            var response = await client.PostAsync($"{settings.ApiUrl}/torrents/file", content);

            var json = await response.Content.ReadAsStringAsync();
            var torrent = JsonConvert.DeserializeObject<Torrent>(json);

            return torrent;
        }

        public async Task<Torrent> AddMagnetTorrentAsync(string magnet)
        {
            var content = new Magnet
            {
                Value = magnet
            };

            var response = await client.PostAsync($"{settings.ApiUrl}/torrents/magnet",
                new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));

            var json = await response.Content.ReadAsStringAsync();
            var torrent = JsonConvert.DeserializeObject<Torrent>(json);

            return torrent;
        }

        public async Task RemoveTorrentAsync(string hash)
        {
            await client.DeleteAsync($"{settings.ApiUrl}/torrents/{hash}");
        }

        public async Task RestartAsync()
        {
            await client.PostAsync($"{settings.ApiUrl}/auth/restart", null);
        }
    }
}
